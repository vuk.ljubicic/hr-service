package com.ebf.hr;

import com.ebf.hr.dao.CompanyDao;
import com.ebf.hr.dao.EmployeeDao;
import com.ebf.hr.domain.Company;
import com.ebf.hr.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;


@Component
public class DataGenerator implements ApplicationRunner {

    private CompanyDao companyDao;
    private EmployeeDao employeeDao;
    @Value("${ebf.data-generation.company-count}")
    private Integer companyCount;
    @Value("${ebf.data-generation.employee-per-company-count}")
    private Integer employeePerCompanyCount;
    @Value("${ebf.data-generation.auto-generate}")
    private Boolean autoGenerate;

    @Autowired
    public DataGenerator(CompanyDao companyDao, EmployeeDao employeeDao) {
        this.companyDao = companyDao;
        this.employeeDao = employeeDao;
    }

    public void run(ApplicationArguments args) {
        if (autoGenerate) {
            Random random = new Random();
            Integer maxSalary = 1000000;
            Integer minSalary = 800000;
            for (int i = 1; i <= companyCount; i++) {
                Company company = Company
                        .builder()
                        .name("Company" + i).build();
                companyDao.save(company);
                for (int j = 1; j <= employeePerCompanyCount; j++) {
                    Employee employee = Employee.builder()
                    .name("Name" + i + j)
                    .surname("Surname" + i + j)
                    .address("Test Street " + i + j)
                    .email("test" + i + j + "@test.com").id(UUID.randomUUID().toString())
                    .salary(random.nextInt(maxSalary - minSalary + 1) + minSalary)
                    .company(company).build();
                    employeeDao.save(employee);
                }
            }
        }
    }
}
