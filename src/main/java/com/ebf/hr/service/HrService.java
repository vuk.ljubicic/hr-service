package com.ebf.hr.service;

import com.ebf.hr.controller.HrServiceErrorCodes;
import com.ebf.hr.controller.HrServiceException;
import com.ebf.hr.controller.v1.vo.CompanyVo;
import com.ebf.hr.controller.v1.vo.EmployeeVo;
import com.ebf.hr.dao.CompanyDao;
import com.ebf.hr.dao.EmployeeDao;
import com.ebf.hr.domain.Company;
import com.ebf.hr.domain.Employee;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
@Transactional
@Slf4j
public class HrService {
    private EmployeeDao employeeDao;
    private CompanyDao companyDao;

    @Autowired
    public HrService(EmployeeDao employeeDao, CompanyDao companyDao) {
        this.employeeDao = employeeDao;
        this.companyDao = companyDao;
    }

    private void validateEmployeeVo(EmployeeVo employeeVo) {
        if (isBlank(employeeVo.getCompanyName())) {
            throw new HrServiceException(HrServiceErrorCodes.MISSING_COMPANY_NAME);
        }
        if (isBlank(employeeVo.getEmail())) {
            throw new HrServiceException(HrServiceErrorCodes.MISSING_EMAIL_ADDRESS);
        }
        if (!EmailValidator.getInstance().isValid(employeeVo.getEmail())) {
            throw new HrServiceException(HrServiceErrorCodes.INVALID_EMAIL_ADDRESS);
        }
    }

    public EmployeeVo updateEmployee(EmployeeVo employeeVo) throws HrServiceException {
        validateEmployeeVo(employeeVo);
        List<Company> companyList = companyDao.findByName(employeeVo.getCompanyName());
        if (companyList.isEmpty()) {
            throw new HrServiceException(HrServiceErrorCodes.UNKNOWN_COMPANY);
        }

        Employee employee = buildEmployee(employeeVo, companyList.get(0));

        List<Employee> employeeList = employeeDao.findByEmail(employee.getEmail());
        if (!employeeList.isEmpty()) {
            employeeList.get(0).populate(employee);
            employee = employeeList.get(0);
        }

        employee = employeeDao.save(employee);
        return buildEmployeeVo(employee);
    }

    public EmployeeVo deleteEmployee(String email) {
        if (isBlank(email) || !EmailValidator.getInstance().isValid(email)) {
            throw new HrServiceException(HrServiceErrorCodes.INVALID_EMAIL_ADDRESS);
        }
        List<Employee> employeeList = employeeDao.findByEmail(email);
        if (employeeList.isEmpty()) {
            throw new HrServiceException(HrServiceErrorCodes.MISSING_EMPLOYEE);
        }
        Employee employee = employeeList.get(0);
        employeeDao.delete(employee);
        return buildEmployeeVo(employee);
    }

    public List<EmployeeVo> listEmployees() {
        List<EmployeeVo> result = new ArrayList<>();
        employeeDao.findAll().forEach(e -> result.add(buildEmployeeVo(e)));
        return result;
    }

    public CompanyVo createCompany(String companyName) {
        List<Company> companyList = companyDao.findByName(companyName);
        if (!companyList.isEmpty()) {
            return CompanyVo.builder()
                    .name(companyList.get(0).getName()).build();
        }
        Company company = companyDao.save(Company.builder().name(companyName).build());
        return CompanyVo.builder()
                .name(company.getName()).build();
    }

    public CompanyVo listCompanyEmployees(String companyName) {
        List<Company> companyList = companyDao.findByName(companyName);
        if (companyList.isEmpty()) {
            throw new HrServiceException(HrServiceErrorCodes.UNKNOWN_COMPANY);
        }
        Company company = companyList.get(0);
        CompanyVo companyVo = CompanyVo.builder()
                .name(company.getName())
                .build();
        List<EmployeeVo> employeeVos = new ArrayList<>();
        employeeDao.findByCompany(company).forEach(e -> employeeVos.add(buildEmployeeVo(e)));
        companyVo.setEmployees(employeeVos);
        return companyVo;
    }

    public Integer salaryAverage(String companyName) {
        List<Company> companyList = companyDao.findByName(companyName);
        if (companyList.isEmpty()) {
            throw new HrServiceException(HrServiceErrorCodes.UNKNOWN_COMPANY);
        }
        return employeeDao.getAverageSalaryForCompany(companyList.get(0).getIdInternal());
    }

    private EmployeeVo buildEmployeeVo(Employee employee) {
        return EmployeeVo.builder()
                .address(employee.getAddress())
                .name(employee.getName())
                .salary(employee.getSalary())
                .surname(employee.getSurname())
                .companyName(employee.getCompany().getName())
                .id(employee.getId())
                .email(employee.getEmail()).build();
    }

    private Employee buildEmployee(EmployeeVo employeeVo, Company company) {
        return Employee.builder()
                .address(employeeVo.getAddress())
                .id(UUID.randomUUID().toString())
                .company(company)
                .email(employeeVo.getEmail())
                .name(employeeVo.getName())
                .surname(employeeVo.getSurname())
                .salary(employeeVo.getSalary())
                .build();
    }
}
