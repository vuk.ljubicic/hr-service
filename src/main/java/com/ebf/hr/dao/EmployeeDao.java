package com.ebf.hr.dao;

import com.ebf.hr.domain.Company;
import com.ebf.hr.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeDao extends JpaRepository<Employee, Integer> {
    List<Employee> findByEmail(String email);

    List<Employee> findByCompany(Company company);

    @Query(value = "select avg(e.salary) from employee e where e.company_id = :id",
            nativeQuery = true)
    Integer getAverageSalaryForCompany(@Param("id") Integer id);
}
