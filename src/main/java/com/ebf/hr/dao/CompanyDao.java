package com.ebf.hr.dao;

import com.ebf.hr.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyDao extends JpaRepository<Company, Integer> {
    List<Company> findByName(String name);
}
