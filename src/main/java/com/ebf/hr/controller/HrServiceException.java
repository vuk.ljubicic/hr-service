package com.ebf.hr.controller;

import java.util.ArrayList;
import java.util.List;

public class HrServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private List<String> errorCodes = null;

    public HrServiceException(HrServiceErrorCodes errorCodes) {
        super(errorCodes.getMessage());
        this.errorCodes = new ArrayList<>();
        addErrorMessage(errorCodes.getCode().toString());
        addErrorMessage(errorCodes.getMessage());
    }

    public List<String> getErrorCodes() {
        return errorCodes;
    }

    public void addErrorMessage(String error) {
        this.errorCodes.add(error);
    }

    public void addErrorMessages(List<String> errors) {
        addErrorMessages(errors.toArray(new String[errors.size()]));
    }

    public void addErrorMessages(String[] errors) {
        for (String error : errors) {
            addErrorMessage(error);
        }
    }

    public String getMessage() {
        StringBuilder str = new StringBuilder();
        for (String error : errorCodes) {
            str.append(error).append(" ");
        }
        return str.toString();
    }
}
