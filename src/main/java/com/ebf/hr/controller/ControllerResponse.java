package com.ebf.hr.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.NoArgsConstructor;


import java.util.Collection;
import java.util.Date;
import java.util.List;

@JsonSerialize
@NoArgsConstructor
public class ControllerResponse<T> {
	@JsonProperty
	private Header header;
	@JsonProperty private T body;

	ControllerResponse(Header header, T body) {
		this.header = header;
		this.body = body;
	}

	public Header getHeader() {
		return header;
	}

	@JsonInclude(JsonInclude.Include.ALWAYS)
	public T getBody() {
		return body;
	}

	@JsonSerialize
    @NoArgsConstructor
    public static class Header {
		private boolean success;
		private Date dateTime;
		private Collection<String> errorCodes;

		public Header(boolean success, Date dateTime, List<String> errorCodes) {
			this.success = success;
			this.dateTime = new Date(dateTime.getTime());
			this.errorCodes = errorCodes;
		}

		public boolean isSuccess() {
			return success;
		}

		public String getDateTime() {
			return dateTime.toString();
		}

		public Collection<String> getErrorCodes() {
			return errorCodes;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		public void setDateTime(Date dateTime) {
			this.dateTime = new Date(dateTime.getTime());
		}

		public void setErrorCodes(List<String> errorCodes) {
			this.errorCodes = errorCodes;
		}

		@Override
		public String toString() {
			return String.format("Header [success=%s, errorCodes=%s, dateTime=%s]", success, errorCodes, dateTime);
		}
	}

	@Override
	public String toString() {
		return "ServiceResponse [header=" + header + ", body=" + body + "]";
	}
}
