package com.ebf.hr.controller;

public enum HrServiceErrorCodes {

    UNKNOWN_ERROR(100, "Unknown error occurred"),
    CLIENT_ABORTED(101, "Client aborted"),
    REQUIRED_PARAMETER_MISSING(102, "Required parameter missing"),
    TYPE_MISMATCH(103, "Argument Type Mismatch"),

    MISSING_EMAIL_ADDRESS(201, "Missing required field - email"),
    MISSING_COMPANY_NAME(202, "Missing required field - companyName"),
    UNKNOWN_COMPANY(203, "Unknown company specified in field - companyName"),
    INVALID_EMAIL_ADDRESS(204, "Invalid email address sent"),
    MISSING_EMPLOYEE(205, "Employee could not be found for email address");


    private Integer code;
    private String message;

    HrServiceErrorCodes(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
