package com.ebf.hr.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.io.IOException;

@ControllerAdvice
@Slf4j
public class ExceptionHandlingAdvice {

    @org.springframework.web.bind.annotation.ExceptionHandler(value = {MissingServletRequestParameterException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ControllerResponse<Void> handleMissingParameterException(Exception exception) {
        log.error("MissingServletRequestParameterException", exception);
        HrServiceException hrServiceException = new HrServiceException(HrServiceErrorCodes.REQUIRED_PARAMETER_MISSING);
        return ControllerResponseBuilder.error(hrServiceException);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ControllerResponse<Void> handleMissmatchException(Exception exception) {
        log.error("MissingServletRequestParameterException", exception);
        HrServiceException hrServiceException = new HrServiceException(HrServiceErrorCodes.TYPE_MISMATCH);
        return ControllerResponseBuilder.error(hrServiceException);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = {Exception.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ControllerResponse<Void> handleException(Exception exception) {
        log.error("Internal Server Error 500 / 0", exception);
        HrServiceException hrServiceException = new HrServiceException(HrServiceErrorCodes.UNKNOWN_ERROR);
        return ControllerResponseBuilder.error(hrServiceException);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = {IOException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ControllerResponse<Void> handleClientAbortRequest(IOException exception) {
        log.error("An io exception occurred", exception);
        HrServiceException hrServiceException = new HrServiceException(HrServiceErrorCodes.CLIENT_ABORTED);
        if (exception.getCause() instanceof ClientAbortException) {
            return null;
        }
        return ControllerResponseBuilder.error(hrServiceException);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = {HrServiceException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ControllerResponse<Void> handleHrServiceException(HrServiceException exception) {
        log.error("Hr service exception occurred", exception);
        return ControllerResponseBuilder.error(exception);
    }
}

