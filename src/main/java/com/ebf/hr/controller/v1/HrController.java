package com.ebf.hr.controller.v1;

import com.ebf.hr.controller.ControllerResponse;
import com.ebf.hr.controller.ControllerResponseBuilder;
import com.ebf.hr.controller.v1.vo.CompanyVo;
import com.ebf.hr.controller.v1.vo.EmployeeVo;
import com.ebf.hr.service.HrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.OK;

@RestController("hrControllerV1")
@RequestMapping(value = "/v1")
public class HrController {
    private HrService hrService;

    public static final String ENDPOINT_EMPLOYEE_UPDATE = "/employee";
    public static final String ENDPOINT_EMPLOYEE_DELETE = "/employee/{email}";
    public static final String ENDPOINT_EMPLOYEE_LIST = "/employee-list";
    public static final String ENDPOINT_COMPANY = "/company/{companyName}";
    public static final String ENDPOINT_SALARY_AVERAGE_COMPANY = "/salary-average/{companyName}";

    @Autowired
    public HrController(HrService hrService) {
        this.hrService = hrService;
    }

    @ResponseStatus(value = ACCEPTED)
    @PostMapping(ENDPOINT_EMPLOYEE_UPDATE)
    public ControllerResponse<EmployeeVo> updateEmployee(@RequestBody(required = true) EmployeeVo employeeVo) {
        return ControllerResponseBuilder.success(hrService.updateEmployee(employeeVo));
    }

    @ResponseStatus(value = ACCEPTED)
    @DeleteMapping(ENDPOINT_EMPLOYEE_DELETE)
    public ControllerResponse<EmployeeVo> deleteEmployee(@PathVariable(required = true) String email) {
        return ControllerResponseBuilder.success(hrService.deleteEmployee(email));
    }

    @ResponseStatus(value = OK)
    @GetMapping(ENDPOINT_EMPLOYEE_LIST)
    public ControllerResponse<List<EmployeeVo>> listEmployees() {
        return ControllerResponseBuilder.success(hrService.listEmployees());
    }

    @ResponseStatus(value = OK)
    @GetMapping(ENDPOINT_COMPANY)
    public ControllerResponse<CompanyVo> getCompany(@PathVariable(required = true) String companyName) {
        return ControllerResponseBuilder.success(hrService.listCompanyEmployees(companyName));
    }

    @ResponseStatus(value = ACCEPTED)
    @PutMapping(ENDPOINT_COMPANY)
    public ControllerResponse<CompanyVo> putCompany(@PathVariable(required = true) String companyName) {
        return ControllerResponseBuilder.success(hrService.createCompany(companyName));
    }

    @ResponseStatus(value = OK)
    @GetMapping(ENDPOINT_SALARY_AVERAGE_COMPANY)
    public ControllerResponse<Integer> salaryAverage(@PathVariable(required = true) String companyName) {
        return ControllerResponseBuilder.success(hrService.salaryAverage(companyName));
    }
}
