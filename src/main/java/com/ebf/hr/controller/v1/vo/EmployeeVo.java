package com.ebf.hr.controller.v1.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class EmployeeVo {
    private String id;
    private String name;
    private String surname;
    private String email;
    private String address;
    private Integer salary;
    private String companyName;
}
