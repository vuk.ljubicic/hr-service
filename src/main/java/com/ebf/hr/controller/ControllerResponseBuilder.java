package com.ebf.hr.controller;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ControllerResponseBuilder {

	private ControllerResponseBuilder() {
	}
	public static <T> ControllerResponse<T> success(T body) {
		return new ControllerResponse<>(buildSuccessHeader(), body);
	}

	public static <T> ControllerResponse<T> error(HrServiceException exception) {
		return new ControllerResponse<>(buildErrorHeader(exception.getErrorCodes()), null);
	}

	private static ControllerResponse.Header buildErrorHeader(List<String> errorCodes) {
		return new ControllerResponse.Header(false, new Date(), errorCodes);
	}

	private static ControllerResponse.Header buildSuccessHeader() {
		return new ControllerResponse.Header(true, new Date(), Collections.<String>emptyList());
	}
}
