package com.ebf.hr.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "company")
public class Company implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "id_internal")
    private Integer idInternal;
    @Column(unique = true)
    private String name;
    @OneToMany(mappedBy = "company")
    private Set<Employee> employees;

    public Company(Integer idInternal, String name) {
        this.idInternal = idInternal;
        this.name = name;
    }

    public Company() {

    }

    public String getName() {
        return name;
    }

    public Integer getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(Integer idInternal) {
        this.idInternal = idInternal;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public static CompanyBuilder builder() {
        return new CompanyBuilder();
    }

    public static class CompanyBuilder {
        private Integer id;
        private String name;

        public CompanyBuilder id(final Integer id) {
            this.id = id;
            return this;
        }

        public CompanyBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public Company build() {
            return new Company(id, name);
        }
    }
}
