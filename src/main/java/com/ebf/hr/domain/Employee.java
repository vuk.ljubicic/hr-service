package com.ebf.hr.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "id_internal")
    private Integer idInternal;
    @Column(unique = true)
    private String id;
    private String name;
    private String surname;
    @Column(unique = true)
    private String email;
    private String address;
    private Integer salary;
    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    public Employee(Integer idInternal, String id, String name, String surname, String email,
                    String address, Integer salary, Company company) {
        this.idInternal = idInternal;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.salary = salary;
        this.company = company;
        this.id = id;
    }

    public Employee() {

    }

    public void populate(Employee employee) {
        company = employee.getCompany();
        salary = employee.getSalary();
        email = employee.getEmail();
        address = employee.getAddress();
        name = employee.getName();
        surname = employee.getSurname();
    }

    public Integer getIdInternal() {
        return idInternal;
    }

    public void setIdInternal(Integer idInternal) {
        this.idInternal = idInternal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public static EmployeeBuilder builder() {
        return new EmployeeBuilder();
    }

    public static class EmployeeBuilder {
        private String id;
        private String name;
        private String surname;
        private String email;
        private String address;
        private Integer salary;
        private Company company;

        public EmployeeBuilder id(final String id) {
            this.id = id;
            return this;
        }

        public EmployeeBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public EmployeeBuilder surname(final String surname) {
            this.surname = surname;
            return this;
        }

        public EmployeeBuilder email(final String email) {
            this.email = email;
            return this;
        }

        public EmployeeBuilder address(final String address) {
            this.address = address;
            return this;
        }

        public EmployeeBuilder salary(final Integer salary) {
            this.salary = salary;
            return this;
        }

        public EmployeeBuilder company(final Company company) {
            this.company = company;
            return this;
        }

        public Employee build() {
            return new Employee(null, id, name, surname, email, address, salary, company);
        }
    }
}
