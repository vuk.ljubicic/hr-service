package com.ebf.hr.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE",
        "spring.datasource.driverClassName=org.h2.Driver"
})
@ActiveProfiles(profiles = "local")
@Transactional(propagation = Propagation.REQUIRES_NEW)
@Sql("classpath:test-data.sql")
@AutoConfigureMockMvc
public abstract class BaseControllerTest {
    protected static final ObjectMapper MAPPER = new ObjectMapper();
    protected static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
                    Charset.forName("utf8"));
    @Autowired
    protected MockMvc mockMvc;

    protected byte[] toBytes(Object vo) throws IOException {
        Map<String, Object> map = MAPPER.convertValue(vo, new TypeReference<Map<String, Object>>() {
        });
        return MAPPER.writeValueAsBytes(map);
    }
}
