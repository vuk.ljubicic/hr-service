package com.ebf.hr.controller.v1;

import com.ebf.hr.controller.BaseControllerTest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HrControllerListEmployeesTest extends BaseControllerTest {
    @Test
    public void shouldListAllEmployees() throws Exception {
        mockMvc.perform(
                get("/v1/" + HrController.ENDPOINT_EMPLOYEE_LIST)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.header.success", is(true)))
                .andExpect(jsonPath("$.body[0].name", is("Employee1")))
                .andExpect(jsonPath("$.body[1].name", is("Employee2")))
                .andExpect(jsonPath("$.body[2].name", is("Employee3")))
                .andExpect(jsonPath("$.body[3].name", is("Employee5")))
                .andExpect(jsonPath("$.body[4].name", is("Employee6")))
                .andExpect(jsonPath("$.body[5].name", is("Employee7")));
    }
}
