package com.ebf.hr.controller.v1;

import com.ebf.hr.controller.BaseControllerTest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HrControllerCreateCompanyTest extends BaseControllerTest {
    @Test
    public void shouldCreateNonExistingCompany() throws Exception {
        String companyName = "CompanyX";
        mockMvc.perform(
                put("/v1/" + HrController.ENDPOINT_COMPANY, companyName)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.header.success", is(true)))
                .andExpect(jsonPath("$.body.name", is("CompanyX")));
    }
}
