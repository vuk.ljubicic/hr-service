package com.ebf.hr.controller.v1;

import com.ebf.hr.controller.BaseControllerTest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HrControllerAverageSalaryTest extends BaseControllerTest {
    @Test
    public void shouldReturnAverageSalaryForExistingCompany() throws Exception {
        String companyName = "Company1";
        mockMvc.perform(
                get("/v1/" + HrController.ENDPOINT_SALARY_AVERAGE_COMPANY, companyName)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.header.success", is(true)))
                .andExpect(jsonPath("$.body", is(100418)));
    }

    @Test
    public void shouldNotReturnAverageSalaryForNonExistingCompany() throws Exception {
        String companyName = "CompanyX";
        mockMvc.perform(
                get("/v1/" + HrController.ENDPOINT_SALARY_AVERAGE_COMPANY, companyName)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("203")))
                .andExpect(jsonPath("$.header.errorCodes[1]",
                        is("Unknown company specified in field - companyName")));
    }
}
