package com.ebf.hr.controller.v1;

import com.ebf.hr.controller.BaseControllerTest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HrControllerDeleteEmployeeTest extends BaseControllerTest {

    @Test
    public void shouldDeleteExistingEmployee() throws Exception {
        String existingEmployeeEmail = "example1@example.com";
        mockMvc.perform(
                delete("/v1/" + HrController.ENDPOINT_EMPLOYEE_DELETE, existingEmployeeEmail)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.header.success", is(true)))
                .andExpect(jsonPath("$.body.name", is("Employee1")))
                .andExpect(jsonPath("$.body.email", is("example1@example.com")));
    }

    @Test
    public void shouldNotDeleteEmployeeForInvalidEmail() throws Exception {
        String invalidEmail = "example1example.com";
        mockMvc.perform(
                delete("/v1/" + HrController.ENDPOINT_EMPLOYEE_DELETE, invalidEmail)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .param("email", invalidEmail))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("204")))
                .andExpect(jsonPath("$.header.errorCodes[1]", is("Invalid email address sent")));
    }

    @Test
    public void shouldNotDeleteEmployeeIfNonExistingEmail() throws Exception {
        String nonExistingEmployeeEmail = "non-existing@example.com";
        mockMvc.perform(
                delete("/v1/" + HrController.ENDPOINT_EMPLOYEE_DELETE, nonExistingEmployeeEmail)
                        .accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .param("email", nonExistingEmployeeEmail))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("205")))
                .andExpect(jsonPath("$.header.errorCodes[1]",
                        is("Employee could not be found for email address")));
    }
}
