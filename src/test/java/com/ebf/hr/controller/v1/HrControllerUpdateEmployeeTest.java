package com.ebf.hr.controller.v1;

import com.ebf.hr.controller.BaseControllerTest;
import com.ebf.hr.controller.v1.vo.EmployeeVo;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class HrControllerUpdateEmployeeTest extends BaseControllerTest {

    @Test
    public void shouldSaveNewEmployee() throws Exception {
        mockMvc.perform(
                post("/v1/" + HrController.ENDPOINT_EMPLOYEE_UPDATE).accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(createValidNewEmployeeVo()))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.header.success", is(true)))
                .andExpect(jsonPath("$.body.name", is("Employee4")));
    }

    @Test
    public void shouldUpdateExistingEmployee() throws Exception {
        mockMvc.perform(
                post("/v1/" + HrController.ENDPOINT_EMPLOYEE_UPDATE).accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(createValidExistingEmployeeVo()))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.header.success", is(true)))
                .andExpect(jsonPath("$.body.name", is("Employee4")));
    }

    @Test
    public void shouldNotSaveEmployeeWithMissingCompany() throws Exception {
        mockMvc.perform(
                post("/v1/" + HrController.ENDPOINT_EMPLOYEE_UPDATE).accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(createInvalidMissingCompanyEmployeeVo()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("202")))
                .andExpect(jsonPath("$.header.errorCodes[1]",
                        is("Missing required field - companyName")));
    }

    @Test
    public void shouldNotSaveEmployeeWithMissingEmail() throws Exception {
        mockMvc.perform(
                post("/v1/" + HrController.ENDPOINT_EMPLOYEE_UPDATE).accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(createInvalidMissingEmailEmployeeVo()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("201")))
                .andExpect(jsonPath("$.header.errorCodes[1]", is("Missing required field - email")));
    }

    @Test
    public void shouldNotSaveEmployeeWithInvalidEmail() throws Exception {
        mockMvc.perform(
                post("/v1/" + HrController.ENDPOINT_EMPLOYEE_UPDATE).accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(createInvalidEmailEmployeeVo()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("204")))
                .andExpect(jsonPath("$.header.errorCodes[1]", is("Invalid email address sent")));
    }


    @Test
    public void shouldNotSaveEmployeeWithUnknownCompany() throws Exception {
        mockMvc.perform(
                post("/v1/" + HrController.ENDPOINT_EMPLOYEE_UPDATE).accept(APPLICATION_JSON_UTF8)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(createInvalidUnknownCompanyEmployeeVo()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.header.success", is(false)))
                .andExpect(jsonPath("$.header.errorCodes[0]", is("203")))
                .andExpect(jsonPath("$.header.errorCodes[1]",
                        is("Unknown company specified in field - companyName")));
    }

    private byte[] createInvalidUnknownCompanyEmployeeVo() throws IOException {
        EmployeeVo employeeVo = EmployeeVo
                .builder()
                .address("Test Address")
                .email("example4@example.com")
                .companyName("CompanyX")
                .name("Employee4")
                .surname("Employee4")
                .salary(30000)
                .build();
        return toBytes(employeeVo);
    }

    private byte[] createValidNewEmployeeVo() throws IOException {
        EmployeeVo employeeVo = EmployeeVo
                .builder()
                .address("Test Address")
                .email("example4@example.com")
                .companyName("Company1")
                .name("Employee4")
                .surname("Employee4")
                .salary(30000)
                .build();
        return toBytes(employeeVo);
    }

    private byte[] createInvalidMissingCompanyEmployeeVo() throws IOException {
        EmployeeVo employeeVo = EmployeeVo
                .builder()
                .address("Test Address")
                .email("example4@example.com")
                .name("Employee4")
                .surname("Employee4")
                .salary(30000)
                .build();
        return toBytes(employeeVo);
    }

    private byte[] createInvalidMissingEmailEmployeeVo() throws IOException {
        EmployeeVo employeeVo = EmployeeVo
                .builder()
                .address("Test Address")
                .companyName("Company1")
                .name("Employee4")
                .surname("Employee4")
                .salary(30000)
                .build();
        return toBytes(employeeVo);
    }

    private byte[] createInvalidEmailEmployeeVo() throws IOException {
        EmployeeVo employeeVo = EmployeeVo
                .builder()
                .address("Test Address")
                .companyName("Company1")
                .email("example4example.com")
                .name("Employee4")
                .surname("Employee4")
                .salary(30000)
                .build();
        return toBytes(employeeVo);
    }

    private byte[] createValidExistingEmployeeVo() throws IOException {
        EmployeeVo employeeVo = EmployeeVo
                .builder()
                .address("Test Address")
                .email("example3@example.com")
                .companyName("Company1")
                .name("Employee4")
                .surname("Employee4")
                .salary(30000)
                .build();
        return toBytes(employeeVo);
    }
}
