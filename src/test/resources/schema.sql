CREATE TABLE company (
  id_internal INT PRIMARY KEY,
  name VARCHAR(250)
);
CREATE TABLE employee (
  id_internal INT PRIMARY KEY,
  id VARCHAR(250),
  name VARCHAR(250),
  surname VARCHAR(250),
  email VARCHAR(250),
  address VARCHAR(250),
  salary INT,
  company_id INT,
  CONSTRAINT fk_company FOREIGN KEY (company_id)
  REFERENCES company(id_internal)
);
CREATE UNIQUE INDEX uidx_company_name
ON company (name);
CREATE UNIQUE INDEX uidx_employee_email
ON employee (email);
CREATE UNIQUE INDEX uidx_employee_id
ON employee (id);