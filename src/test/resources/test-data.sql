delete from company where id_internal=100;
insert into `company` (`id_internal`, `name`) values
(100, 'Company1');

delete from employee where id_internal=100;
insert into `employee` (`id`, `id_internal`, `name`, `surname`, `email`, `address`, `salary`, `company_id`) values
('id1', 100, 'Employee1', 'Employee1', 'example1@example.com', 'Address1', 51255, 100);

delete from employee where id_internal=200;
insert into `employee` (`id`, `id_internal`, `name`, `surname`, `email`, `address`, `salary`, `company_id`) values
('id2',200, 'Employee2', 'Employee2', 'example2@example.com', 'Address2', 100000, 100);

delete from employee where id_internal=300;
insert into `employee` (`id`, `id_internal`, `name`, `surname`, `email`, `address`, `salary`, `company_id`) values
('id3', 300, 'Employee3', 'Employee3', 'example3@example.com', 'Address3', 150000, 100);

delete from company where id_internal=200;
insert into `company` (`id_internal`, `name`) values
(200, 'Company2');

delete from employee where id_internal=400;
insert into `employee` (`id`, `id_internal`, `name`, `surname`, `email`, `address`, `salary`, `company_id`) values
('id5', 400, 'Employee5', 'Employee5', 'example5@example.com', 'Address5', 60000, 200);

delete from employee where id_internal=500;
insert into `employee` (`id`, `id_internal`, `name`, `surname`, `email`, `address`, `salary`, `company_id`) values
('id6', 500, 'Employee6', 'Employee6', 'example6@example.com', 'Address6', 110000, 200);

delete from employee where id_internal=600;
insert into `employee` (`id`, `id_internal`, `name`, `surname`, `email`, `address`, `salary`, `company_id`) values
('id7', 600, 'Employee7', 'Employee7', 'example7@example.com', 'Address7', 160000, 200);
