FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/hr-service-1.0.0.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","-Debf.data-generation.auto-generate=true","/app.jar"]