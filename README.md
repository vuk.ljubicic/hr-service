# EBF HR Service

Service provides REST API for managing employee and company data.
It uses in memory DB2 database to store data to a local db file.
It has functionality to populate database with configurable number of company and employee demo data.
To test and view specification of endpoints, Swagger UI is included as well as DB2 UI console.

## Building the service (javac)

- Have Java 8 and latest version of maven installed

- Clone the repository

- In project directory execute `mvn clean install`

- Verify that `./target/hr-service-1.0.0.jar` is present

## Running the service (java)

 - To run the service locally on default port (8888) execute

`java -jar hr-service-1.0.0.jar` from `./target` directory

 - To generate company and employee demo data execute with

`java -Debf.data-generation.auto-generate=true -jar hr-service-1.0.0.jar`

By default 10 companies with 10 employees each will be generated

- To change default port execute with

`java -Dserver.port=${PORT_VALUE} -jar hr-service-1.0.0.jar`

${PORT_VALUE} - replace with desired port

## Building the service (docker)

 - Follow 'Building the service (javac)' section

 - Have latest version of Docker installed

 - Change directory to project directory

 - Execute `docker build -t ebf/hr-service:v1 .`

## Running the service (docker)

 - Execute `docker run -d -p 8888:8888 ebf/hr-service:v1`

## Exposed UI URLs

- Swagger UI: http://${HOST}:${PORT}/swagger-ui.html
- DB2 UI: http://${HOST}:${PORT}/h2-console

Replace ${HOST}:${PORT} with you local values i.e http://localhost:8888

To login to DB2 console use presented login credentials (un:sa pw: no pass), set JDBC URL to `jdbc:h2:./ebf/hr`

